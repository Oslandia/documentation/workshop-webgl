# WebGL

En action sur les gitlab pages: https://oslandia.gitlab.io/documentation/workshop-webgl/

## Getting started

Servir les fichiers d'une façon ou d'une autre:

- utiliser le paquet npm `http-server`.
- ou le module python `http.server`
- ou un outils comme codepen.io !
- ou autre chose 

## Dessiner 3 points

Servir le code ci-dessous et le modifier pour que 3 points soient dessinés dans le canvas.

```html
<!doctype html>
<html>
   <body>
      <canvas width = "570" height = "570" id = "my_Canvas"></canvas>

      <script>
         /*================Creating a canvas=================*/
         var canvas = document.getElementById('my_Canvas');
         gl = canvas.getContext('experimental-webgl'); 

         /*==========Defining and storing the geometry=======*/

         var vertices = [
             // TODO
         ];

         // Create an empty buffer object to store the vertex buffer
         var vertex_buffer = gl.createBuffer();

         //Bind appropriate array buffer to it
         gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);

         // Pass the vertex data to the buffer
         gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

         // Unbind the buffer
         gl.bindBuffer(gl.ARRAY_BUFFER, null);

         /*=========================Shaders========================*/

         // vertex shader source code
         var vertCode =
            'attribute vec3 coordinates;' +

            'void main(void) {' +
               ' gl_Position = vec4(coordinates, 1.0);' +
               'gl_PointSize = 10.0;'+
            '}';

         // Create a vertex shader object
         var vertShader = gl.createShader(gl.VERTEX_SHADER);
 
         // Attach vertex shader source code
         gl.shaderSource(vertShader, vertCode);

         // Compile the vertex shader
         gl.compileShader(vertShader);

         // fragment shader source code
         var fragCode =
            'void main(void) {' +
               ' gl_FragColor = vec4(0.0, 0.0, 0.0, 0.1);' +
            '}';

         // Create fragment shader object
         var fragShader = gl.createShader(gl.FRAGMENT_SHADER);

         // Attach fragment shader source code
         gl.shaderSource(fragShader, fragCode);

         // Compile the fragmentt shader
         gl.compileShader(fragShader);

         // Create a shader program object to store
         // the combined shader program
         var shaderProgram = gl.createProgram();

         // Attach a vertex shader
         gl.attachShader(shaderProgram, vertShader); 

         // Attach a fragment shader
         gl.attachShader(shaderProgram, fragShader);

         // Link both programs
         gl.linkProgram(shaderProgram);

         // Use the combined shader program object
         gl.useProgram(shaderProgram);

         /*======== Associating shaders to buffer objects ========*/

         // Bind vertex buffer object
         gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);

         // Get the attribute location
         var coord = gl.getAttribLocation(shaderProgram, "coordinates");

         // Point an attribute to the currently bound VBO
         gl.vertexAttribPointer(coord, 3, gl.FLOAT, false, 0, 0);

         // Enable the attribute
         gl.enableVertexAttribArray(coord);

         /*============= Drawing the primitive ===============*/

         // Clear the canvas
         gl.clearColor(0.5, 0.5, 0.5, 0.9);

         // Enable the depth test
         gl.enable(gl.DEPTH_TEST);
 
         // Clear the color buffer bit
         gl.clear(gl.COLOR_BUFFER_BIT);

         // Set the view port
         gl.viewport(0,0,canvas.width,canvas.height);

         // Draw the triangle
         gl.drawArrays(gl.POINTS, 0, 3);
      </script>
   </body>
</html>
```

## Dessiner un triangle

Quelle modif sont à faire ?

## dessiner un rectangle

### Avec 6 sommets

- Il faut ajouter 3 vertices. Comment ?
- et il y a une autre modification à faire: laquelle ? -> le count dans drawArrays

### Avec seulement 4 sommets et les index buffer

- Essayez en ne rajoutant que 1 vertex:
    - 4 vertices en tout
    - définir un index buffer:
        - type Uint16Array
        - bind/bufferData: gl.ELEMENT_ARRAY_BUFFER
    - avant le draw:
        - rebind l'index buffer
        - `drawArray` -> `drawElement` (:warning: 1 argument en plus !)

## Ajouter de la couleur

- modifiez la couleur du rectangle, directement depuis le shader
- dynamiser cette couleur avec un uniform (NOTE: ajouter `precision highp float;` au début du shader)
  ```js
  var colorLoc = gl.getUniformLocation(shaderProgram, 'colors');
  gl.uniform3f(colorLoc, r, g, b);
  ```
- en mobilisant les concepts vu précédemment, comment feriez vous un gradient interpolé entre les couleurs de chaque vertex ?

## Transformer la géométrie

Sans modifier les vertex:

- Décaler le triangle légèrement vers la droite
- Dessiner-le 2x plus grand

De même que pour la couleur, essayer de passer les valeurs depuis un uniform.

Quel est l'intérêt ?

## Mettre en mouvement

- Mettre en place une boucle de rendu, puis faire une petite oscillation
- optionnel: faite le tourner de manière continue

# Corrigés

https://gitlab.com/Oslandia/documentation/workshop-webgl/

# Pour aller plus loin

- [tutoriel](https://www.tutorialspoint.com/webgl/)
- ["quick" reference](https://www.slideshare.net/Khronos_Group/webgl-20-reference-guide)
- [Sur MDN](https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API) (bien pour l'API js)
- doc Open GL ES 2.0: [référence](https://registry.khronos.org/OpenGL-Refpages/es2.0/) et [glsl référence](https://www.khronos.org/opengles/sdk/docs/manglsl/docbook4/)
- Support sur [caniuse](https://caniuse.com/webgl)
